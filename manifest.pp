#-------------------------------------------------------------------------------------------------
# Manifest Api-Investimento
#-------------------------------------------------------------------------------------------------

#
# Módulos:
# mod 'puppetlabs-java', '6.3.0'
# mod 'puppet-nginx', '1.1.0'
# mod 'puppetlabs-apt', '7.5.0'
# mod 'puppetlabs-sshkeys_core', '2.1.0'

#
# Instruções do arquivo api-investimento.service
$svc_conf_template = @(END)
[Unit]
Description=Api-Investimentos

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/Api-Investimentos/Api-Investimentos-0.0.1-SNAPSHOT.jar

[Install]
WantedBy=multi-user.target
END

#
# Instalação do Java 8
class java {
  package {'openjdk-8-jdk':
    ensure => installed,
  }
}

#
# Configuração do Daemon-Reload
class systemd::daemon_reload {
  exec {'/usr/bin/systemctl daemon-reload':
    refreshonly => true,
  }
}

#
# Criação do diretório para receber o jar da aplicação
class artefato {
  file {'/home/ubuntu/Api-Investimentos':
    ensure => directory,
    owner  => 'ubuntu',
    group  => 'ubuntu',
    mode   => '644',
  }
}

# Criação do serviço api-investimentos.service
class api_investimento {
  file {'/etc/systemd/system/api-investimentos.service':
    ensure  => file,
    content => inline_epp($svc_conf_template),
  }
  service {'api-investimentos.service':
    ensure => running,
    enable => true,
  }
}

#
# Instalação e configuração do Nginx
class instala_nginx {
  include ::systemd::daemon_reload
  include nginx

  nginx::resource::server {'ronaldosr.com':
    listen_port => 80,
    proxy       => 'http://localhost:8080',
  }

  file {'/etc/nginx/conf.d/default.conf':
    ensure => absent,
    notify => Service['nginx'],
  }
}

#
# Configuração da chave de acesso ao server
class chave_ssh {
  ssh_authorized_key {'jenkins@banana':
    ensure => present,
    user   => 'ubuntu',
    type   => 'ssh-rsa',
    key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb'
  }
}

include java
include artefato
include api_investimento
include instala_nginx
include chave_ssh

