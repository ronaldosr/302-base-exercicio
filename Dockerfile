FROM openjdk:8u121-jre-alpine
COPY target/Api-Investimentos-*.jar /app/Api-Investimentos.jar
WORKDIR /app
CMD ["java", "-jar", "Api-Investimentos.jar"]