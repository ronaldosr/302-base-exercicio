pipeline {
    agent any
    parameters {
        string(
            name: "HOST_USER",
            defaultValue: "ubuntu")
        string(
            name: "HOST_IP",
            defaultValue: "3.128.27.153")
        string(
            name: "JAR_NAME",
            defaultValue: "Api-Investimentos-*.jar")
        string(
            name: "APP_DIR",
            defaultValue: "/home/ubuntu/Api-Investimentos")
        string(
            name: "SERVICE_NAME",
            defaultValue: "api-investimentos.service"
        )
        string(
            name: "REGISTRY_URL",
            defaultValue: "registry-itau.mastertech.com.br"
        )
        string(
            name: "MYIMAGE",
            defaultValue: "api_invest_ronaldo"
        )
    }

    options {
        gitLabConnection("gitlab")
    }

    post {
        always {
            cleanWs()
        }
        success {
            echo "======>  Tudo certo. Aplicação no ar :-D"
            updateGitlabCommitStatus name: "build", state: "success"
        }
        failure {
            echo "======> Falha no processo de deploy :-("
            updateGitlabCommitStatus name: "build", state: "failed"
        }
    }
    
    stages {
        stage("Test"){
            steps {
                echo "======> Executando testes"
                sh "./mvnw test"
            }
        }
        stage("Build") {
            when{branch "master"}
            /*
            Todos os blocos de stage (Package, Build and push image e Deploy serão executados apenas
            quando o evento disparado pelo SCM for na branch master.
            */
            stages{
                stage("Package") {
                    steps {
                        script{
                            echo "======> Empacotando a aplicação"
                            sh "./mvnw clean package"
                        }
                    }
                }
                stage("Build and push image"){
                    steps{
                        script {
                            echo "======> Registrando a imagem ${params.REGISTRY_URL}/${params.MYIMAGE}"
                            docker.withRegistry("https://${params.REGISTRY_URL}","registry_credential"){
                                def customImage=docker.build("${params.REGISTRY_URL}/${params.MYIMAGE}")
                                /* Os dois 'push' abaixo fazem o push da imagem para o registry com a tag latest e a tag do numero do build da pipeline */
                                echo "======> Fazendo o push da imagem ${params.REGISTRY_URL}/${params.MYIMAGE}"
                                customImage.push("${env.BUILD_ID}")
                                customImage.push("latest")
                            }
                        }
                    }
                }
                stage("Deploy"){
                    steps{
                        echo "======> Fazendo pull da imagem ${params.REGISTRY_URL}/${params.MYIMAGE}:latest"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.HOST_USER}@${params.HOST_IP} 'docker pull ${params.REGISTRY_URL}/${params.MYIMAGE}:latest'"
                        
                        echo "======> Parando a imagem ${params.MYIMAGE}"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.HOST_USER}@${params.HOST_IP} 'docker stop ${params.MYIMAGE}'"
                        
                        echo "======> Iniciando a image ${params.MYIMAGE}"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.HOST_USER}@${params.HOST_IP} 'docker run --name ${params.MYIMAGE} --rm -p 5000:8080 -d ${params.REGISTRY_URL}/${params.MYIMAGE}'"
                    }
                }
            }
        }
    }
}
